golang-github-hashicorp-go-slug (0.9.1-2) unstable; urgency=medium

  * reintroduce patch (Closes: #1030575)
    it is still needed for autpkgtest

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 05 Feb 2023 11:31:50 +0100

golang-github-hashicorp-go-slug (0.9.1-1) unstable; urgency=medium

  [ Thorsten Alteholz ]
  * debian/control: bump standard to 4.6.2

  [ Aloïs Micard ]
  * update debian/gitlab-ci.yml (using pkg-go-tools/ci-config)

  [ Mathias Gibbens ]
  * Team upload
  * New upstream version 0.9.1 (Closes: #997847)
    - Drop patch that skipped some tests (no longer seems to be needed)
    - Remove unneeded Files-Excluded from d/copyright
    - d/control:
      - Update Standards-Version to 4.6.1 (no changes needed)
      - Update Section to golang
      - Add Rules-Requires-Root: no
      - Add Multi-Arch: foreign

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 02 Feb 2023 18:31:50 +0100

golang-github-hashicorp-go-slug (0.7.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control: use dh13
  * debian/control: bump standard to 4.6.0
  * reverse dependencies successfully built with ratt:
    - golang-github-hashicorp-go-tfe

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 05 Oct 2021 18:31:50 +0000

golang-github-hashicorp-go-slug (0.5.0-2) unstable; urgency=medium

  * let package pass both "go test" during package build and autopkgtest/CI

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 14 Jan 2021 23:09:45 +0000

golang-github-hashicorp-go-slug (0.5.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Thorsten Alteholz ]
  * New upstream release: fix for CVE-2020-29529 (Closes: #976873)
  * reverse dependencies successfully built with ratt:
    - nothing todo for this package

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 10 Dec 2020 20:27:48 +0000

golang-github-hashicorp-go-slug (0.4.1-1) unstable; urgency=medium

  * Initial release

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 02 Dec 2019 21:50:05 +0000
